from django.shortcuts import render
from django.views.generic import TemplateView, ListView
from .models import Post

class HomePageView(ListView):
    template_name = 'home.html'
    #context_objext_name = 'all_posts_list'
    model = Post