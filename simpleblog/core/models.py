from django.db import models
import datetime

class Post (models.Model):
    title = models.CharField(max_length=200)
    body = models.TextField()
    #author
    date = models.DateField(("Date"), default=datetime.date.today)
    #score
    def __str__(self):
        return self.title

